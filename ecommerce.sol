// SPDX-License-Identifier: GPL-3.0

pragma solidity >=0.7.0 <0.9.0;


/** 
 * @title Ballot
 * @dev Implements voting process along with vote delegation
 */
contract ecommerce {

    address _acheteur;
    address _vendeur;
    mapping(address => uint256) _balances;
    uint _prix;

    event ListProduits (string produit, uint quantite);
    event ProduitsAchetes(address _from, address _to, uint price);

    enum values { ProduitsDisponibles, ArticlesCommandes }
    
    constructor(uint256 total, address vendeur) {
        _balances[msg.sender] = total;
        _vendeur = vendeur;
    }

    function balanceOf(address tokenOwner) public view returns (uint) {
        return _balances[tokenOwner];
    }

    function achat(address acheteur, uint prix) public {
        // vérifier que l'acheteur a assez de tokens
        require(prix <= _balances[acheteur]);
        emit ListProduits("Produit", 1);
        paiement(acheteur, prix);
    }

    function paiement( address acheteur, uint prix) public {
        // Transfert acheteur > vendeur
        _balances[acheteur] -= prix;
        _balances[_vendeur] += prix;
        // Emetre msg
        emit ProduitsAchetes(_vendeur, acheteur, prix);
    }
}
